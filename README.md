# Belgian Postal Codede

Belgium has a constantly changing postal code system because of
municipalities who are merging with each other.

This module contains a migration script to automatically fill a
taxonomy vocabulary with the Main municipality and sub municipalities.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/belgian_postal_code).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/belgian_postal_code).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Dependencies

- Taxonomy
- Migrate
- Migrate Plus
- Migrate Spreadsheet
- Migrate Tools


## Pre-requisites

You must have configured the Drupal private file system **before** installing this module.


## Usage

Simply install the module and let the migration process do its thing.

You can manually execute the migrations one by one again with the following commands:

```bash
drush migrate-import belgian_postal_code_main_municipalities
drush migrate-import belgian_postal_code_sub_municipalities
```

Or you can use the simple drush command provided by this module to download a new file and re-import all data (in update mode).

```bash
drush belgian_postal_codes:update
```


## Uninstalling

The module does not delete any of the imported data. If you want to delete
the vocabulary or terms this will be a manual action from your side.


## Maintainers

- Bram Driesen - [BramDriesen](https://www.drupal.org/u/bramdriesen)
- Christophe Goffin - [cgoffin](https://www.drupal.org/u/cgoffin)