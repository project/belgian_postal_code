<?php

/**
 * @file
 * Contains all hook implementations for the belgian_postal_code module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function belgian_postal_code_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.belgian_postal_code':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Creates and fills a taxonomy vocabulary with all the municipalities in Belgium (Flanders, Walloon and Brussels)') . '</p>';
      return $output;

    default:
  }
}

/**
 * Migrate callback to map the source id to the corresponding destination id.
 *
 * @param array $values
 *
 * @return int|null
 */
function belgian_postal_code_destination_mapper(array $values): ?int {
  [$migration, $source_id] = $values;

  $query = Drupal::database()->select('migrate_map_' . $migration, 'migration')
    ->fields('migration', ['destid1'])
    ->distinct()
    ->condition('migration.sourceid1', $source_id)
    ->isNotNull('migration.destid1');

  if (count($values) === 3) {
    $query->condition('migration.sourceid2', array_pop($values));
  }

  $destination_id = $query->execute()->fetchField();
  if (!$destination_id) {
    return NULL;
  }

  return $destination_id;
}
