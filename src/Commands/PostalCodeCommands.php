<?php

namespace Drupal\belgian_postal_code\Commands;

use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\belgian_postal_code\Helper\PostalCodeHelperInterface;
use Drush\Commands\DrushCommands;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;

/**
 * Adds drush commands for our postal codes.
 */
class PostalCodeCommands extends DrushCommands implements SiteAliasManagerAwareInterface {
  use SiteAliasManagerAwareTrait;

  /**
   * The postal code helper interface.
   *
   * @var \Drupal\belgian_postal_code\Helper\PostalCodeHelperInterface
   */
  protected PostalCodeHelperInterface $postalCodeHelper;

  /**
   * The postal code helper commands constructor.
   *
   * @param \Drupal\belgian_postal_code\Helper\PostalCodeHelperInterface $postalCodeHelper
   *   The postalcode helper interface.
   */
  public function __construct(PostalCodeHelperInterface $postalCodeHelper) {
    parent::__construct();
    $this->postalCodeHelper = $postalCodeHelper;
  }

  /**
   * Updates the vocabulary of municipalities.
   *
   * @command belgian_postal_codes:update
   * @aliases belgian_postal_codes-update
   */
  public function updateMuncipalities() {
    $this->postalCodeHelper->downloadPostalCodeFile();

    $process = $this->processManager()->drush($this->siteAliasManager()->getSelf(), 'migrate-import', ['belgian_postal_code_main_municipalities']);
    $process->mustRun();

    $process = $this->processManager()->drush($this->siteAliasManager()->getSelf(), 'migrate-import', ['belgian_postal_code_sub_municipalities']);
    $process->mustRun();
  }

}
