<?php

namespace Drupal\belgian_postal_code\Helper;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Helper class for the Belgian Postal Codes module.
 */
class PostalCodeHelper implements PostalCodeHelperInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The zipcodes URL from bpost.
   *
   * Unfortunately this is currently hosted on an HTTP only server.
   *
   * @see: https://www.bpost.be/nl/postcodevalidatie-tool
   */
  protected const ZIPCODES_URL = "http://www.bpost2.be/zipcodes/files/zipcodes_num_nl_new.xls";

  /**
   * PostalCodeHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function downloadPostalCodeFile(): mixed {
    $download_result = system_retrieve_file(self::ZIPCODES_URL, 'private://import/belgian_postal_codes_nl.xls', FALSE, FileSystemInterface::EXISTS_REPLACE);

    if (!$download_result) {
      throw new \Exception('Failed to download zipcodes file. No migrations have been executed. Check the logs for more details.');
    }

    return $download_result;
  }

  /**
   * {@inheritdoc}
   */
  public function getMunicipalitiesByPostalCode(string $postalCode): array {
    $term_ids = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('field_postal_code_be', $postalCode)
      ->execute();

    if ($term_ids) {
      return $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadMultiple($term_ids);
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPostalCodeByMunicipality(string $municipality): ?TermInterface {
    $terms = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadByProperties([
        'name' => $municipality,
        'vid' => 'belgian_postal_code',
      ]);

    if ($terms) {
      return reset($terms);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMainMunicipalityByPostalCode(string $postalCode) {
    // @todo Implement.
  }

  /**
   * {@inheritdoc}
   */
  public function getMainMunicipalityBySubMunicipality(string $subMunicipality) {
    // @todo Implement.
  }

}
