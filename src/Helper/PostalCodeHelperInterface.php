<?php

namespace Drupal\belgian_postal_code\Helper;

use Drupal\taxonomy\TermInterface;

/**
 * Helper interface for the Belgian Postal Codes module.
 */
interface PostalCodeHelperInterface {

  /**
   * Downloads the postalcode file to the private filesystem.
   *
   * @return mixed
   *   One of these possibilities:
   *   - If it succeeds and $managed is FALSE, the location where the file was
   *     saved.
   *   - If it succeeds and $managed is TRUE, a \Drupal\file\FileInterface
   *     object which describes the file.
   *   - If it fails, FALSE.
   *
   * @throws \Exception
   */
  public function downloadPostalCodeFile(): mixed;

  /**
   * Loads all municipalities by a given postal code.
   *
   * It's not possible to return a single municipality this way because in
   * Belgium it is possible that multiple cities share the same postal code.
   *
   * @param string $postalCode
   *   The postal code as string.
   *
   * @return array
   *   An array of postal code terms if there are results.
   */
  public function getMunicipalitiesByPostalCode(string $postalCode): array;

  /**
   * Function which returns a postal code term by a given municipality name.
   *
   * @param string $municipality
   *   Municipality name.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The term if a result is found.
   */
  public function getPostalCodeByMunicipality(string $municipality): ?TermInterface;

  /**
   * Gets the main municipality based off a postal code.
   *
   * @param string $postalCode
   *   The postal code.
   */
  public function getMainMunicipalityByPostalCode(string $postalCode);

  /**
   * Gets the main municipality by a sub-municipality.
   *
   * @param string $subMunicipality
   *   The sub-municipality.
   */
  public function getMainMunicipalityBySubMunicipality(string $subMunicipality);

}
